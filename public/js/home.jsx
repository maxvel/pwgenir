'use strict';

var alphabet = "!#%+23456789:=?@ABCDEFGHJKLMNPRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

var Irrational = React.createClass({
    render: function() {
        var value = this.props.value;
        var offset = this.props.offset;
        var size = this.props.size;
        if(offset < 300) {
            var before = value.slice(0, offset);
        } else {
            var before = value.slice(offset-300, offset);
        }

        var selected = value.slice(offset, offset+size);
        var after = value.slice(offset+size, offset+size+300);
        return (
            <span style={{wordWrap: "break-word"}}>
                {before}<span style={{color: "red"}}>{selected}</span>{after}
            </span>
        );
    }
});

var PassGen = React.createClass({
    render: function() {
        var password = convertBase(this.props.data, 10, alphabet);
        return <span><input value={password} /></span>
    }
});

var Main = React.createClass({
    getInitialState: function() {
        var keys = _.keys(numbers);
        return {"current_number": keys[Math.floor(Math.random()*keys.length)],
                "offset": Math.round(Math.random() * 50000),
                "size": 25}
    },
    onNumberChange: function(event) {
        this.setState({"current_number": event.target.value});
    },
    onOffsetChange: function(event) {
        this.setState({offset: parseInt(event.target.value)});
    },
    onSizeChange: function(event) {
        this.setState({size: parseInt(event.target.value)});
    },
    render: function() {
        var self = this;
        var number_values = _.pairs(this.props.numbers).map(function(p) {
            return <option value={p[0]}>{p[0]} ({p[1].length} digits)</option>;
        });
        var number = this.props.numbers[this.state.current_number];
        var selected = number.slice(this.state.offset, this.state.offset + this.state.size);
        return (
            <span>
                <h2>Generate your super secure password!</h2>
                <select onChange={this.onNumberChange} value={this.state.current_number}>{number_values}</select>
                Offset: <input type="number" min="0" onChange={this.onOffsetChange} value={this.state.offset} />
                Size: <input type="number" min="0" onChange={this.onSizeChange} value={this.state.size} />
                Password: <PassGen data={selected} />
                <br /><br />
                <Irrational value={number} size={this.state.size} offset={this.state.offset} />
                <br /><br />
                Alphabet: <input size="100" value={alphabet} />
            </span>
        );
    }
});

ReactDOM.render(<Main numbers={numbers} />, document.body);
